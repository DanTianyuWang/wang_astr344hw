'''
HW10 Extracredit
'''

import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import scipy as scipy
data_a = np.loadtxt('hw10a.dat')
data_b = np.loadtxt('hw10b.dat')

a_col1 = [data_a[i][0] for i in range(len(data_a))]
a_col2 = [data_a[i][1] for i in range(len(data_a))]
b_col1 = [data_b[i][0] for i in range(len(data_b))]
b_col2 = [data_b[i][1] for i in range(len(data_b))]

def gaussian(x, mu, sigma, a, c): # Un-normalized Gaussian distribution function
	return a*np.exp(-(x-mu)**2/(2*sigma**2))+c

def double_gaussian(x, mu_1,sigma_1, a_1, mu_2, sigma_2, a_2, c):
	return (a_1*np.exp(-(x-mu_1)**2/(2*sigma_1**2))+
		    a_2*np.exp(-(x-mu_2)**2/(2*sigma_2**2))+
		    c)

def trapz_integrate(x_list, y_list): # Trapezoidal integrator using numpy function
	# inputs are the interval of integration and the values of a function over the interval
	# output is a number, the value of integration of the function over the range
	return np.trapz(y_list, x_list)

#a_col2 = scipy.stsci.convolve.boxcar(a_col2, (5,), mode='constant')
popt_a,pcov_a = curve_fit(gaussian, a_col1, a_col2, p0=[50,50,0.02,0]) 

print '----------------------------------'
print 'hw10b.dat'
x_a = np.linspace(-1000, 1000, 1000)
y_a = gaussian(x_a, popt_a[0], popt_a[1], popt_a[2], popt_a[3])
print 'The FWHM is '+str(2.355*popt_a[1]) # FWHM = 2.355*sigma
print 'Area under the single Gaussian curve is '+str(popt_a[2]*popt_a[1]*np.sqrt(2*np.pi))# area = a*sigma*sqrt(2pi)
line1, = plt.plot(a_col1,a_col2,'bo')
line2, = plt.plot(x_a, y_a,'r')
plt.xlabel('velocity (km/s)')
plt.ylabel('temperature')
plt.axis([-800,800,-0.01,0.025])
plt.legend([line1, line2],['data points', 'single Gaussian fit'], loc = 'best')
plt.title('hw10a.dat')
plt.show()


print '----------------------------------'
print 'hw10b.dat'
popt_b1,pcov_b1 = curve_fit(gaussian, b_col1, b_col2,p0=[0,50,0.02,-0.01])
popt_b2,pcov_b2 = curve_fit(double_gaussian, b_col1, b_col2, p0=[-50,20,0.02,50,20,0.02,-0.01])
x_b = np.linspace(-300,300,1000)
y_b1 = gaussian(x_b, popt_b1[0], popt_b1[1], popt_b1[2],popt_b1[3]) 
y_b2 = double_gaussian(x_b,popt_b2[0],popt_b2[1],popt_b2[2],popt_b2[3],popt_b2[4],popt_b2[5],popt_b2[6]) 
print 'The FWHM is '+str(2.355*popt_b1[1]) # FWHM = 2.355*sigma
print 'Area under the single Gaussian curve is '+str(popt_b1[2]*popt_b1[1]*np.sqrt(2*np.pi))# area = a*sigma*sqrt(2pi)

line3, = plt.plot(b_col1,b_col2,'bo',markersize=3)
line4, = plt.plot(x_b,y_b1,'g') # single Gaussian
line5, = plt.plot(x_b,y_b2,'r') # double Gaussian
plt.legend([line3, line4, line5],['data points','single guassian fit', 'double guassian fit'], loc='best')
plt.title('hw10b.dat')
plt.show()
print '----------------------------------'
print 'I did not get the smoothing function to work.'