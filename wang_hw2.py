"""
This is a function that calculates the exponential of a number. Specifically, we are looking at how errors propagates when using single precision floating number. In the recurrence relation, coefficients are assumed using double precision floating numbers because we are only interested in the error inttroduced by the input. 
"""

import numpy as np

def exponential(base, exponent): # Exponent is a non-negative integer
    if exponent == 0 and base != 0:
        return 1
    elif exponent == 1:
        return base
    else: 
        return (13./3.) * exponential(base, exponent-1) - (4./3.) * exponential(base, exponent-2) 

def abs_error(x_real, x_fp): # Gives the absolute error  between real and floating point numbers
    return x_real - x_fp

def rel_error(x_real, x_fp): # Gives the relative error by dividing the absolute error by x_real
    return (x_real - x_fp) / x_real 

base_sp = np.float32(1./3.)
base_dp = np.float64(1./3.)
exponent = 5
print str(base_sp)+" to the "+ str(exponent)+" is "+str(exponential(base_sp, exponent))+" using single precision floating number." 
print str(base_dp)+" to the "+ str(exponent)+" is "+str(exponential(base_dp, exponent))+" using double precision floating number."
print "The absolute error for " + str(base_sp)+" to the "+ str(exponent)+ " is "+ str(abs_error(exponential(base_dp, exponent),exponential(base_sp, exponent)))
print "The relative error for " + str(base_sp)+" to the "+ str(exponent)+ " is "+ str(rel_error(exponential(base_dp, exponent),exponential(base_sp, exponent)))


print ""
exponent = 20
print str(base_sp)+" to the "+ str(exponent)+" is "+str(exponential(base_sp, exponent))+" using single precision floating number."
print str(base_dp)+" to the "+ str(exponent)+" is "+str(exponential(base_dp, exponent))+" using double precision floating number."
print "The absolute error for " + str(base_sp)+" to the "+ str(exponent)+ " is "+ str(abs_error(exponential(base_dp, exponent),exponential(base_sp, exponent)))
print "The relative error for " + str(base_sp)+" to the "+ str(exponent)+ " is "+ str(rel_error(exponential(base_dp, exponent),exponential(base_sp, exponent)))

print ""
base_sp = np.float32(4.)
base_dp = np.float64(4.)
exponent = 20
print str(base_sp)+" to the "+ str(exponent)+" is "+str(exponential(base_sp, exponent))+" using single precision floating number."
print str(base_dp)+" to the "+ str(exponent)+" is "+str(exponential(base_dp, exponent))+" using double precision floating number."
print "The absolute error for " + str(base_sp)+" to the "+ str(exponent)+ " is "+ str(abs_error(exponential(base_dp, exponent),exponential(base_sp, exponent)))
print "The relative error for " + str(base_sp)+" to the "+ str(exponent)+ " is "+ str(rel_error(exponential(base_dp, exponent),exponential(base_sp, exponent)))
print ''
print "Neither absolute or relative error can be used to measure accuracy and stability. When we use single precision FP, the value will turn wrong completely at some point in the recurrence relation. Therefore the final value for such exponential is nothing but a meaningless number. It does not indicate the extext to which the error propagates. All we can say is that given such error, it is not accurate and stable using single precision FP but we don't know how inaccurate or instable it is."
 
