"""
This code plots the numerical difference for the columns extracted from the following datasheets. 
"""

import numpy as np
import matplotlib.pyplot as plt
data_1 = np.loadtxt('model_smg.dat') # Model data
data_2 = np.loadtxt('ncounts_850.dat') # Observed data

def num_diff(x_0, x_1, x_2, y_0, y_1, y_2): # function defined to calculate numerical differentiation
    h1 = x_1 - x_0
    h2 = x_2 - x_1
    return h1/(h2*(h1+h2))*y_2 - (h1-h2)/(h1*h2)*y_1 - h2/(h1*(h1+h2))*y_0

diff_list = np.zeros(len(data_1)-2) # list of log10 of differentiation
i = 1
while i < len(data_1)-1:
    diff_list[i-1] = np.log10(-num_diff(data_1[i-1][0], data_1[i][0], data_1[i+1][0], data_1[i-1][1], data_1[i][1], data_1[i+1][1]))
    i = i+1



plt.plot(np.log10(data_1[:,0][1:-1]), diff_list, 'r^', data_2[:,0], data_2[:,1], 'bo')
plt.xlabel('Log(L)')
plt.ylabel('Log(dN/dL)')
plt.show()
plt.savefig('wang_hw3.jpg') # Figure saved cannot be shown