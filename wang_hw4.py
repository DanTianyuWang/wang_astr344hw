'''
Homework 4: Numerical integration using trapezoidal rule
'''

from math import *
from numpy import *
import matplotlib.pyplot as plt

C = 3*10**3 # Register constants
omega_m = 0.3
omega_lambda = 0.7
omega_r = 0
def r(z): # Formula for Hubble distance
	return C/sqrt(omega_m*(1+z)**3+omega_r*(1+z)**2+omega_lambda) 

x1 = range(500)
x2 = [x*0.01 for x in x1]
r_list = [r(x) for x in x2]

def trapz_sum(x_list, y_list): # x_list and y_list should have the same length
	sum_list = zeros(len(x_list))
	sum = 0
	for i in range(len(x_list)-1):
		sum = sum + (y_list[i]+y_list[i+1])*(x_list[i+1]-x_list[i])/2
		sum_list[i] = sum
	return sum_list
	
comov_dis = trapz_sum(x2,r_list)

d_list = zeros(len(x2))
for i in range(len(x2)):
	d_list[i] = comov_dis[i]/(1+x2[i])

d_list_np = zeros(len(x2)) # Use numpy trapz function
for i in range(len(x2)):
    d_list_np[i] = trapz(r_list[:i], x = x2[:i])/(1+x2[i])

line1, = plt.plot(x2[1:-1], d_list[1:-1], 'bo')
line2, = plt.plot(x2[1:-1], d_list_np[1:-1],'r^')
plt.legend([line1, line2],['Numeric integration','np.trapz integration'])
plt.xlabel('Redshift z')
plt.ylabel('Angular diameter distance D')
plt.show()