'''
HW4 Extra credit 
Integration of a function of order 3 using piecewise linear method, trapezoidal integrator, Simpson's rule, and Gaussian Quadrature
'''
import numpy as np


def linear_integrate(x_list, y_list): # Piecewise linear method 
	# inputs are the interval of integration and the values of a function over the interval
	# output is a number, the value of integration of the function over the range
	sum = 0
	for i in range(len(x_list)-1):
		sum = sum + (y_list[i]+y_list[i+1])*(x_list[i+1]-x_list[i])/2
	return sum

def trapz_integrate(x_list, y_list): # Trapezoidal integrator using numpy function
	# inputs are the interval of integration and the values of a function over the interval
	# output is a number, the value of integration of the function over the range
	return np.trapz(y_list, x_list)

def simpson_integrate(x_list, y_list): # Simpson's rule
	# inputs are the interval of integration and the values of a function over the interval
	# output is a number, the value of integration of the function over the range
	i = 0
	sum = 0
	while i+2 < len(x_list):
		small_sum = (x_list[i+1]-x_list[i])* \
						 (1./3.*y_list[i]+4./3.*y_list[i+1]+1./3.*y_list[i+2]) 
		sum = sum + small_sum
		i = i+2
	return sum
def gaussian_integrate(): # Gaussian Quadrature
	# The function that we need to integrate has order 2n-1 = 3, so we need 
	# n = 2 terms in the sum 
	x1 = np.sqrt(1./3.) # Roots of Legendre polynomials for n = 2
	x2 = -np.sqrt(1./3.)
	w1 = 1 # Weights for each legendre polynomial are 1
	w2 = 1
	return w1*f(x1) + w2*f(x2) 

def f(x):
	return x**3 + 2*x**2 - 4

		 
x_list = [x*0.001 for x in range(-1000,1000)] # Interval of integration is [-1,1] with step size 0.001
y_list = [f(x) for x in x_list] # Get values of f(x) in the interval


print 'The exact value of the integral is '+ str(-20./3.)
print 'The value using linear piecewise integral is '+ str(linear_integrate(x_list, y_list))+ ', and the error is '+str(-20./3.-linear_integrate(x_list, y_list))
print 'The value using trapezoidal integrator is '+ str(trapz_integrate(x_list, y_list))+ ', and the error is '+str(-20./3.-trapz_integrate(x_list, y_list))
print "The value using Simpson's rule is "+ str(simpson_integrate(x_list, y_list))+ ', and the error is '+str(-20./3.-simpson_integrate(x_list, y_list))
print "The value using Gaussian Quadrature is "+ str(gaussian_integrate())+ ', and the error is '+str(-20./3.-gaussian_integrate())
