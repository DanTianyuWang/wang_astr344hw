'''
HW5 Root finding of arbritary functions using bisection method.
This algorithm only works for continuous function. User has to assure that there is only one root in the given bracket.
Otherwise the user will be asked to change the bracket (either double or half the bracket).
'''

import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime 
from datetime import timedelta 


def f(x):
	# Roots from 2 to 4 id 3.14...., pi
	return np.sin(x)

def g(x):
	# Root from 1 to 2 is 1.52138...
	return (x**3)-x-2

def y(x):
	# Roots from 0 to 5 is 2
	return -6+x+(x**2)

def is_there_a_root(function, bracket):
	# True if there is a root in the bracket interval
	check_bracket = False 
	if function(bracket[0])*function(bracket[1]) < 0:
		check_bracket = True


	return check_bracket	    
def get_root(function, bracket, tolerance, max_run):
	# tweak tolerance and max_run if result is not accurate
	
	
	count  = 0
	while np.absolute(function(bracket[0])) > np.absolute(tolerance) and count <= max_run:
		# get root at bracket[0]
		count += 1
		mid_point = (bracket[0] + bracket[1]) / 2.
		if function(bracket[0])*function(mid_point) < 0:
			bracket[1] = mid_point
		else:
			bracket[0] = mid_point
	return bracket[0] # The root

function_list = [f, g, y]
bracket_list = [[2,4], [1,2], [0,5]]  # Must change both bracket_list and bracket_list_copy here.
bracket_list_copy = [[2,4],[1,2],[0,5]] # I tried bracket_list_copy = bracket_list[:] to make them refer to different objects but they both point to the same object. If one of them is changed another also changes.
analytic_root = [np.pi, 1.52138, 2.]
tolerance = 10**(-5) # Change your tolerance here
max_run = 10**5 # Change the maximum run times here


for i in range(len(function_list)):
	#is_there_a_root(function_list[i], bracket_list[i])
	x_list = np.zeros((bracket_list[i][1]-bracket_list[i][0])*1000)
	y_list = np.zeros((bracket_list[i][1]-bracket_list[i][0])*1000)
	for j in range((bracket_list[i][1]-bracket_list[i][0])*1000):
		x_list[j] = bracket_list[i][0] + j/1000.
		y_list[j] = function_list[i](x_list[j])
	plt.figure()
	plt.ylabel(function_list[i].__name__)
	plt.xlabel('x')
	plt.plot(x_list, y_list)
	plt.axhline(0, color='black')
	plt.title(function_list[i].__name__+'(x)')
	
	if is_there_a_root(function_list[i], bracket_list_copy[i]):

		print 'The root for function ' + function_list[i].__name__ + ' is ' + \
			      str(get_root(function_list[i], bracket_list[i], tolerance, max_run)) + \
			      ' in the range ' + str(bracket_list_copy[i][0]) + ' to ' + str(bracket_list_copy[i][1]) +\
			      ', and the error is ' + str(analytic_root[i]-get_root(function_list[i], bracket_list_copy[i], tolerance, max_run))
	else:
		print 'There might not be a root in this interval, try the interval from ' + \
		      str(bracket_list_copy[i][0]-(bracket_list_copy[i][1]-bracket_list_copy[i][0])/2.) + ' to ' + str(bracket_list_copy[i][1]+(bracket_list_copy[i][1]-bracket_list_copy[i][0])/2.) +\
		      ' or ' + str(bracket_list_copy[i][0]+(bracket_list_copy[i][1]-bracket_list_copy[i][0])/4.) + ' to ' + str(bracket_list_copy[i][1]-(bracket_list_copy[i][1]-bracket_list_copy[i][0])/4.)
	
plt.show()
print "-----------------------------"

tic = datetime.now()
get_root(f, [2,4], tolerance, max_run)
toc = datetime.now()
print "The run time for function f is "+str(toc-tic)+"s."
print "-----------------------------"

tic = datetime.now()
get_root(g, [1,2], tolerance, max_run)
toc = datetime.now()
print "The run time is "+str(toc-tic)+"s."
print "-----------------------------"

tic = datetime.now()
get_root(y, [0,5], tolerance, max_run)
toc = datetime.now()
print "The run time is "+str(toc-tic)+"s."
print "-----------------------------"



