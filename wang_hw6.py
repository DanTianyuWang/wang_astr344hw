'''
HW6 
Part 1
Root finding using Newton-Raphson method. Same functions are used as in HW5. Functions are analytically differentiable.
'''

import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime 
from datetime import timedelta 



def f(x):
	# Roots from 2 to 4 id 3.14...., pi
	return np.sin(x)

def g(x):
	# Root from 1 to 2 is 1.52138...
	return (x**3)-x-2

def y(x):
	# Roots from 0 to 5 is 2
	return -6+x+(x**2)

def derivative(function, x, delta_x):
	# Derivative of a continuous function specified by delta_x
	# Derivative is acuurate if delta_x approaches 0+
	# This is different from the formula for numerical differentiation in HW3 
	return (function(x+delta_x)-function(x-delta_x))/(2*delta_x)

def NR_get_root(function, guess, tolerance, max_run):
	# Newton_Raphson method to get root
	# Works for a "good guess"
	# Adjust tolerance and max_run if needed to
	
	run = 0
	while np.absolute(function(guess)) > np.absolute(tolerance) and run <= max_run:
		guess = guess - function(guess)/derivative(function, guess, 0.01)
		run += 1
	return guess
	


tolerance = 10**(-5) # Change tolerance here
max_run = 10**5 # Change maximum run time here

print "-----------------------------"
print "Part 1"
print "-----------------------------"

tic = datetime.now()
print "A root for function f is "+str(NR_get_root(f, 2, tolerance, max_run))
toc = datetime.now()
print "The run time is "+str(toc-tic)+"s."
print "-----------------------------"

tic = datetime.now()
print "A root for function f is "+str(NR_get_root(g, 1, tolerance, max_run))
toc = datetime.now()
print "The run time is "+str(toc-tic)+"s."
print "-----------------------------"

tic = datetime.now()
print "A root for function f is "+str(NR_get_root(y, 5, tolerance, max_run))
toc = datetime.now()
print "The run time is "+str(toc-tic)+"s."
print "-----------------------------"

print "NR method is faster than bisection method in HW5. Run HW5 to get the run time using bisection method."

'''
Part 2
Finding the Temperature of a Black Body with intensity 1.25e-12 at 870 micron in cgs units.
'''
print "-----------------------------"
print "Part 2"
print "-----------------------------"

wavelength = 870*10**(-4) # Initiate constants in cgs units
c = 2.99792458*10**10 # speed of light
v = c/wavelength # frequency
h = 6.6260755*10**(-27) # Planck constant
k = 1.380658*10**(-16) # Boltzmann constant

def planck_fn(T):
	# planck function minus the given intensity in order to get root
	return (2*h*(v**3))/(c**2*(np.e**((h*v)/(k*T))-1)) - 1.25*10**(-12)


tolerance = 10**(-20)
print "The temperature corresponds to "+str(NR_get_root(planck_fn, 1000, tolerance, max_run))+"K."