'''
HW7
'''

import numpy as np  
'''
Part 1. Calculate pi using Hit or Miss methods.
'''
def pi(N):
	# Give the number of trials of "hit" and "miss" and return the value of approximately pi by
	# counting how many hits are inside a circle of radius 1 if the allowed range is a square of side 2
	
	I = 0 # count hits

	i = 1
	while i <= N:
		x = 2.*np.random.random()
		y = 2.*np.random.random()
		if (x-1.)**2+(y-1.)**2 <= 1:
			I += 1
		i += 1
	return I/(N+0.0)*4.


print "Part A"
print "Get pi=" + str(pi(10**1)) + " for 10 tries."
print "Get pi=" + str(pi(10**2)) + " for 10^2 tries."
print "Get pi=" + str(pi(10**3)) + " for 10^3 tries."
print "Get pi=" + str(pi(10**4)) + " for 10^4 tries."
print "Get pi=" + str(pi(10**5)) + " for 10^5 tries."
print "Get pi=" + str(pi(10**6)) + " for 10^6 tries."
print "Get pi=" + str(pi(10**7)) + " for 10^7 tries." 
print "Sometimes I can get to 3.141 with 10^5 to 10^7 tries."
print "-----------------------------"

'''
Part 2. What is the smallest number of people in a room for the probability to be greater than 0.5 that two
people in the group have the same birthday? (note, the answer is 23 people).
'''

N = 2 # Initiate number of people
N_max = 366 # Maximum number of people needed to have two people of the same birthday
p = 0 # Initiate probability of two people having the same birthday
p_req = 0.5 # Required probability
T = 1000 # number of times run for each number of people to get the probability 

while N < N_max and p <= p_req:
	count = 0 # count the instance where two people have the same birthday
	for i in range(T):
		birthdays = np.random.randint(1, high=366, size=N) # Draw random intergers in [1,366) for everyone to indicate birthday
		unique_birthdays = np.unique(birthdays) # list the unique birthdays
		if not len(birthdays) == len(unique_birthdays):
			count += 1 
	p = (count+0.0)/(T+0.0)
	N += 1

print "Part B"
print "The smallest number is "+str(N-1)+" for the probability to be greater than 0.5 \nthat two people in the group have the same birthday"
print "This number is usually between 22 to 24 and most of the time it is 23."









