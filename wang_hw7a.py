'''
HW7
Part A. Calculate pi using Hit or Miss methods.
'''

import numpy as np  

def pi(N):
	# Give the number of trials of "hit" and "miss" and return the value of approximately pi by
	# counting how many hits are inside a circle of radius 1 if the allowed range is a square of side 2
	
	I = 0 # count hits

	i = 1
	while i <= N:
		x = 2.*np.random.random()
		y = 2.*np.random.random()
		if (x-1.)**2+(y-1.)**2 <= 1:
			I += 1
		i += 1
	return I/(N+0.0)*4.


print "Part A"
print "Get pi=" + str(pi(10**1)) + " for 10 tries."
print "Get pi=" + str(pi(10**2)) + " for 10^2 tries."
print "Get pi=" + str(pi(10**3)) + " for 10^3 tries."
print "Get pi=" + str(pi(10**4)) + " for 10^4 tries."
print "Get pi=" + str(pi(10**5)) + " for 10^5 tries."
print "Get pi=" + str(pi(10**6)) + " for 10^6 tries."
print "Get pi=" + str(pi(10**7)) + " for 10^7 tries." 
print "Sometimes I can get to 3.141 with 10^5 to 10^7 tries."
print "-----------------------------"



