'''
HW7
Part B. What is the smallest number of people in a room for the probability to be greater than 0.5 that two
people in the group have the same birthday? (note, the answer is 23 people).
'''

import numpy as np 

N = 2 # Initiate number of people
N_max = 366 # Maximum number of people needed to have two people of the same birthday
p = 0 # Initiate probability of two people having the same birthday
p_req = 0.5 # Required probability
T = 1000 # number of times run for each number of people to get the probability 

while N < N_max and p <= p_req:
	count = 0 # count the instance where two people have the same birthday
	for i in range(T):
		birthdays = np.random.randint(1, high=366, size=N) # Draw random intergers in [1,366) for everyone to indicate birthday
		unique_birthdays = np.unique(birthdays) # list the unique birthdays
		if not len(birthdays) == len(unique_birthdays):
			count += 1 
	p = (count+0.0)/(T+0.0)
	N += 1

print "Part B"
print "The smallest number is "+str(N-1)+" for the probability to be greater than 0.5 \nthat two people in the group have the same birthday"
print "This number is usually between 22 to 24 and most of the time it is 23."
