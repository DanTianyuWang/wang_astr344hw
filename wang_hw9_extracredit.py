'''
HW9 Extracredit
Linear fit
'''

import numpy as np
import matplotlib.pyplot as plt

def s(sigma):
	# sigma is a 1*N array 
	inverse_squared = np.zeros(len(sigma))
	for i in range(len(sigma)):
		inverse_squared[i] = 1./(sigma[i]**2)
	return sum(inverse_squared)

def sum_x(x,sigma):
	# x and sigma are 1*N arrays, len(x) == len(sigma)
	x_over_sigma_squared = np.zeros(len(x))
	for i in range(len(x)):
		x_over_sigma_squared[i] = x[i]/(sigma[i]**2)
	return sum(x_over_sigma_squared)

def sum_y(y,sigma):
	# y and sigma are 1*N arrays, len(y) == len(sigma)
	y_over_sigma_squared = np.zeros(len(y))
	for i in range(len(y)):
		y_over_sigma_squared[i] = y[i]/(sigma[i]**2)
	return sum(y_over_sigma_squared)

def sum_x_squared(x,sigma):
	# x and sigma are 1*N arrays, len(x) == len(sigma)
	x_squared_over_sigma_squared = np.zeros(len(x))
	for i in range(len(x)):
		x_squared_over_sigma_squared[i] = (x[i]**2)/(sigma[i]**2)
	return sum(x_squared_over_sigma_squared)

def sum_xy(x,y,sigma):
	# x, y and sigma are 1*N arrays, len(x) == len(y) == len(sigma)
	xy_over_sigma_squared = np.zeros(len(x))
	for i in range(len(x)):
		xy_over_sigma_squared[i] = (x[i]*y[i])/(sigma[i]**2)
	return sum(xy_over_sigma_squared)

def linear_fit(x,y,sigma):
	# x, y and sigma are 1*N arrays, len(x) == len(y) == len(sigma)
	
	a_1 = (sum_y(y,sigma)*sum_x_squared(x,sigma)-sum_x(x,sigma)*sum_xy(x,y,sigma))/(s(sigma)*sum_x_squared(x,sigma)-sum_x(x,sigma)**2)
	a_2 = (s(sigma)*sum_xy(x,y,sigma)-sum_y(y,sigma)*sum_x(x,sigma))/(s(sigma)*sum_x_squared(x,sigma)-sum_x(x,sigma)**2)
	x_list = np.arange(-2, 6, 0.001) # Need to change this for specific input
	y_list = [a_1+a_2*num for num in x_list]
	line1, = plt.plot(x, y, 'r^')
	line2, = plt.plot(x_list, y_list, 'bo', markersize = 1)
	plt.legend([line1, line2],['data points','linear fit'], loc='best')
	plt.xlabel('log10(CO brightness)')
	plt.ylabel('log10(starformation rate)')
	plt.show()

data = np.loadtxt('ks_observables.dat', skiprows=1)
x = np.array([data[i][0] for i in range(len(data))])
y = np.array([data[i][1] for i in range(len(data))])

sigma = np.ones(len(x)) # default error is 1 

#print len(np.log10(x)) == len(np.log10(y))

linear_fit(np.array(np.log10(x)),np.array(np.log10(y)),sigma)



