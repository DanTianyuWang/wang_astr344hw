'''
Project1
'''
import numpy as np
import matplotlib.pyplot as plt 
from copy import *

class Person:
	def __init__(self, infected = False, vaccinated = False):
	# initialize x,y positions for a person
	# infected is False by default
		self.x = np.random.random()
		self.y = np.random.random()
		self.infected = infected
		self.vaccinated = vaccinated

	def distance(self, other):
	# gives the distance between self and other
		return np.sqrt((self.x-other.x)**2+(self.y-other.y)**2)

	def buffer_zone(self, other):
	# if self is too close to other, move self out of the buffer zone of other
	# can adjust the size of the buffer 
		buffer_size = 0.01
		if Person.distance(self, other) < buffer_size:
			self.x = self.x + buffer_size*(self.x-other.x)/Person.distance(self, other)
			self.y = self.y + buffer_size*(self.y-other.y)/Person.distance(self, other)
	 

	def move(self, others):
	# others is a list of objects of the same class
	# move to an arbitrary point around its current position
	# can adjust its speed by changing the multiplier
	# create buffer zone for each person
		v_x = np.random.uniform(-1,1) # Generate random velocity
		v_y = np.random.uniform(-1,1)

		if self.x+v_x*0.01 > 1: # Out of range, bounce back
			self.x = 1-(self.x+v_x*0.01-1)
		elif self.x+v_x*0.01 < 0: # Out of range
			self.x = -(self.x+v_x*0.01)
		else:
			self.x = self.x+v_x*0.01

		if self.y+v_y*0.01 > 1: # Out of range, bounce back
			self.y = 1-(self.y+v_y*0.01-1)
		elif self.y+v_y*0.01 < 0: # Out of range
			self.y = -(self.y+v_y*0.01)
		else:
			self.y = self.y+v_y*0.01
		
		# Comment out below if buffer is not needed
		for other in others: # Make sure self is not in any other's buffer zone
			Person.buffer_zone(self, other)

		# Comment out below if infection is not needed
		infect_size = 0.15
		for other in others:
			if (Person.distance(self, other) < infect_size and other.infected == True 
				and self.vaccinated == False and np.random.exponential(1) <= 1):
				self.infected = True
				


N = 100 # Number of people
t_max = 1000 # Maximum time index 
trials = 10
time_to_infect_everyone = np.zeros(trials)
N_vac = [x*5 for x in range(15)]
time_vac = np.zeros(15) # time to infect everyone else for different number of vaccinated people

#for trial in range(trials): # run multiple trials

for n_vac in N_vac: # initially different number of people vaccinated
	# create a list of N persons
	people = []
	for i in range(N): 
		people.append(Person())	
	
	people[0].infected = True # Infect one person

	for i in range(n_vac): # Vaccinate n_vac people from the end of the list
		people[N-i-1].vaccinated = True

	count = np.zeros(t_max) # Count how many people are infected after each move 	
	count[0] = 1 # Initially 1 person is infected


	t = 0 # Initialize time	
	while t < t_max and count[t-1] < N-n_vac: # Continue moves until everyone is infected or exceeds max time
		for i in range(N): # move one by one
			one_person = people[i]
			people_copy = deepcopy(people)
			people_copy.pop(i)
			other_persons = people_copy
			one_person.move(other_persons)
		#line below show how people[1] moves graphically 
		#plt.plot(people[1].x, people[1].y, 'bo', markersize=5)
			if one_person.infected == True:
				count[t+1] += 1
		
		print t, count[t]
		t += 1	

	# Uncomment below to plot one realization
	# plt.step(range(t), count[0:t])
	# plt.xlabel('time index')
	# plt.ylabel('number of people infected')
	# plt.title('One realization of the spread of disease')
	# plt.axis([0,t,0,100])
	# plt.show()

	#time_to_infect_everyone[trial] = t-1
	time_vac[n_vac/5] = t-1

plt.plot(N_vac, time_vac, 'bo')
plt.xlabel('number of people vaccinated initially')
plt.ylabel('time to infect everyone who is not vaccinated')
plt.title('Effect of vaccination to prevent spread of disease')
plt.axis([0,80,0,max(time_vac)+100])
plt.show()
